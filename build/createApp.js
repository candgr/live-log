const app = require('express')();
const express = require('express');
const http = require('http').createServer(app);
const path = require('path')
const io = require('socket.io')(http);

const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/static', express.static('build'))
io.on('connection', (socket) => {
    io.sockets.emit('connection event', `new user connected ( total: ${Object.keys(io.clients().connected).length} )`)
    socket.on('log-outer',(data)=>{
        console.log(data)

        const {
            app,
            source,
            log,
            time,
            type
        } = JSON.parse(data)

        io.sockets.emit('log-recieve', JSON.stringify({
            app,
            source,
            log,
            time,
            type
        }))
    })
    socket.on('disconnect', function () {
        io.sockets.emit('connection event', `user disconnected ( total: ${Object.keys(io.clients().connected).length} )`)
    });

})

module.exports = ({
    config
}) => {
    app.get('/health-check',(req,res) => {
        res.status(200).send('ok').end()
    })
    app.get('/', (req, res) => {
        const {
            key
        } = config
        const {
            token
        } = req.query

        if (token != key) res.status(401).send('Unauthorized').end();

        res.sendFile(__dirname + '/views/index.html')
    })
    app.post('/post-log', (req, res) => {
        const {
            app,
            source,
            log,
            time,
            type,
            token
        } = req.body;
        //if (token != key) res.status(401).send('Unauthorized');
        io.sockets.emit('log-recieve', JSON.stringify({
            app,
            source,
            log,
            time,
            type
        }))
        res.send('ok').end()
    });
    return {
        http
    }
}