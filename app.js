const {
    http
} = require('./build/createApp')({
    config: require('./config')
})
http.listen(3333, () => {
    console.log('listening on *:3333');
})